package main

import (
	"math/rand"
)

const KeyID = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789-_+$~"

func RandomString(length int) string {
	b := make([]uint8, length)
	for i := range b {
		b[i] = KeyID[rand.Intn(len(KeyID))]
	}
	return string(b)
}

func GetNewKey() string {
	return RandomString(6)
}

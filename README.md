# paste

Small temp redis-based pastebin server.

## Table of contents

- [Setup](#setup)
- [Configuration](#configuration)
    - [Configuration example](#configuration)

## Setup

This is a daemon that is meant to run behind a HTTP reverse proxy such as Caddy, Lighttpd, or Haproxy.

> You can use whichever RP you want, though.

You'll need a daemon manager, such as `systemd` or `supervisord`,
to keep it alive in a reliable way.

You'll also need a `redis` server, in which to store temporary pastes.

## Usage

```
$ paste -h
Usage of ./paste:
  -config string
    	Path to directory containing the configuration file (default ".")
```

By default, paste is configured to run in a simple way, without any required
configuration.

If you have a custom configuration store you want to use, you can use the CLI
argument `-config` to provide a custom directory in which paste can find a
`paste.<ext>` configuration file.

## Features

- Ephemeral paste storing (up to 1 day)
- Built-in prometheus stats collecting (see [Prometheus stats](#prometheus-stats))
- Three view modes
    - Source-code (with syntaxic coloration)
    - Markdown rendering mode (HTML-escaped)
    - Raw data (for document retrieval)

## Configuration

By default, Paste uses the [viper](https://github.com/spf13/viper) configuration library,
thus supporting every configuration format viper supports.

However, I'd recommend going for TOML, a pretty simple, but strict, clear,
and well-designed, configuration format.

The following configuration variables are available.

- `Http.Host` (default `127.0.0.1:1234`): HTTP listening interface host
- `Redis.Address` (default `127.0.0.1:6379`): Redis URL
- `Redis.Password` (default ""): Redis password
- `Redis.Database` (default 0): Redis database
- `Prometheus.Enabled` (default `false`): Should the prometheus collector
    server be started?
- `Prometheus.Host` (default `127.0.0.1:1235`): Prometheus collector listening interface host

You should put the configuration file inside the process' working directory,
or provide a path to the folder containing the configuration file using the associated flag.

You can also provide configurations through the environment.

Since the environment keys are case-insensitive, `Http.Host` is the exact same as
`HTTP.HOST`.

The loading order is:

- Environment variable
- In-file value
- Default value

An example configuration can be found below.

### Configuration example

Default values are kept for `Redis.Password` and `Redis.Database`.

The host is configured in a TOML file, put in the PWD.

```toml
# paste.toml
[http]
host = "127.0.0.1:3000"
```

The Redis remote server address is configured through the environment.

```
export REDIS.ADDRESS="127.0.0.1:6379"
```

## Prometheus stats

Paste (since [`v2.0.0`][v2.0.0]) comes with built-in support for Prometheus stats. 

The following stats are available.

- `paste_count` (constant): Number of pastes currently stored
- `paste_total_paste_size` (constant): The total memory used by paste storing (in bytes)
- `paste_average_paste_size` (constant): The average memory used by pastes (in bytes)

[v2.0.0]: https://gitlab.com/Artemix/paste/tree/v2.0.0
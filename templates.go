package main

import (
	"fmt"
	"html/template"
	"strings"

	"gitlab.com/golang-commonmark/markdown"
)

// Takes an input string, counts the number of lines,
// and generates an output with the same number of lines,
// each containing the line number.
func Lineno(input string) string {
	output := ""
	strings.Split(input, "\n")

	for idx, _ := range strings.Split(input, "\n") {
		output += fmt.Sprintf("%d\n", idx+1)
	}

	return output[:len(output)-1]
}

// Markdown allows raw HTML output
func Markdown(input string) template.HTML {
	md := markdown.New(markdown.XHTMLOutput(true))
	return template.HTML(md.RenderToString([]byte(input)))
}

// InitTemplates parse templates from templates/ folder
func InitTemplates() *template.Template {
	return template.Must(template.New("").Funcs(template.FuncMap{
		"lineno":   Lineno,
		"markdown": Markdown,
	}).ParseGlob("templates/*"))
}

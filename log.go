package main

import (
	"log"
	"net/http"
	"os"
)

var (
	_prefix = ProjectName + ":: "
	_status = log.New(os.Stdout, _prefix, 0)
	_error  = log.New(os.Stderr, _prefix, 0)
)

// Logf uses Printf as backend, to write data to stdout
func Logf(format string, data ...interface{}) {
	_status.Printf(format, data...)
}

// ErrIf checks if the err value isn't null, and in which case,
// it logs it on stderr
func ErrIf(err interface{}) {
	if nil != err {
		_error.Println(err)
	}
}

// DieIf checks if the err value isn't null, and in which case, it
// logs it on stderr, then dies with status code 1
func DieIf(err interface{}) {
	if nil != err {
		_error.Println(err)
		os.Exit(1)
	}
}

// StatusRecorder is a custom ResponseWriter type that wraps the standard
// writer, to allow status code extraction.
// Like RW, by default, it uses http.StatusOK (200)
type StatusRecorder struct {
	http.ResponseWriter
	status int
}

// WriteHeader keeps note of the given status code
func (rec *StatusRecorder) WriteHeader(code int) {
	rec.status = code
	rec.ResponseWriter.WriteHeader(code)
}

// LogRequest takes the current request and logs the result:
// IP: PROTOCOL_VERSION STATUS_CODE METHOD "URI"
func LogRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		rec := &StatusRecorder{w, http.StatusOK}

		next.ServeHTTP(rec, req)

		_status.Printf("%s: %s %d %s \"%s\"\n",
			req.RemoteAddr,
			req.Proto,
			rec.status,
			req.Method,
			req.RequestURI,
		)
	})
}

module.exports = {
	theme: {
		extend: {}
	},
	variants: {},
	plugins: [
		require('tailwindcss'),
		require('autoprefixer'),
		require('@fullhuman/postcss-purgecss')({
		  content: [
		    './templates/*.html'
		  ],
		  defaultExtractor: c => c.match(/[A-Za-z0-9-_:/]+/g) || []
		}),
		require('cssnano')
	]
};

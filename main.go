package main

import (
	"flag"
	"gitlab.com/Artemix/paste/exporter"
	"net/http"
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/Artemix/paste/storage"
)

type _CLIArgs struct {
	configDir string
}

func main() {
	// CLI args parsing
	args := _CLIArgs{}
	flag.StringVar(&args.configDir, "config", ".",
		"Path to directory containing the configuration file")
	flag.Parse()

	config, err := LoadConfig(args.configDir)
	ErrIf(err)

	redisBackend, err := storage.NewRedisBackend(&redis.Options{
		Addr:     config.Redis.Address,
		Password: config.Redis.Password,
		DB:       config.Redis.Database,
	})
	DieIf(err)

	handlers := &Handlers{
		Templates:      InitTemplates(),
		BackendService: redisBackend,
	}

	srv := &http.Server{
		Addr:         config.HTTP.Host,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
		Handler:      handlers.GetRouter(),
	}

	if config.Prometheus.Enabled {
		statsServer := exporter.New(redisBackend, config.Prometheus.Host)
		// Run prometheus server on a coroutine
		go func(exporter exporter.Exporter) {
			Logf("Prometheus server about to start listening on %s\n", exporter.Host)
			DieIf(exporter.Server.ListenAndServe())
		}(statsServer)
	}

	Logf("HTTP server about to start listening on %s\n", config.HTTP.Host)
	DieIf(srv.ListenAndServe())
}

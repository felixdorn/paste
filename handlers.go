package main

import (
	"fmt"
	"html/template"
	"net/http"
	"net/url"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/Artemix/paste/storage"
	"gitlab.com/Artemix/validator-go"
	"gitlab.com/Artemix/validator-go/rules"
)

// region Data Structures and types

// Handlers is the base request handlers struct, including dependencies
type Handlers struct {
	BackendService storage.Backend
	Templates      *template.Template
}

// Content is the response data structure
type Content struct {
	Error string
	Value string
	// Those following string fields are the paste in its different view modes (raw, view, markdown, etc.)
	// on a URL-prefix-basis, e.g. View may contain `/s/<code>`, raw would contain `/r/<code>`,
	// and md would contain `/md/<code>`
	View            string
	Raw             string
	Md              string
	OriginalCommand PasteSubmitCommand
}

// PasteSubmitCommand is the paste upload command
type PasteSubmitCommand struct {
	Paste          string
	TimeStr        string
	ExpirationTime time.Duration
}

// PasteSubmitSchema is the schema used to validate form submission
var PasteSubmitSchema = map[string][]validator.Rule{
	"paste": {rules.Required{}},
	"time":  {rules.Required{}, rules.Choice{Choices: []string{"1h", "6h", "12h", "24h"}}},
}

// GenericError is representing generic HTTP errors
type GenericError struct {
	Title       string
	Description string
}

var genericErrors = map[int]GenericError{
	http.StatusBadRequest:          {"Bad request", "You sent a request the server cannot handle."},
	http.StatusNotFound:            {"Page not found", "This document could not be found. Maybe it expired?"},
	http.StatusInternalServerError: {"Internal server error", "The server crashed during the request."},
}

// endregion

// region Router

// GetRouter builds a HTTP router based on Gorilla/Mux, to handle
// HTTP traffic
func (h Handlers) GetRouter() *mux.Router {
	r := mux.NewRouter()
	r.Use(LogRequest)

	r.HandleFunc("/", h.handleNewPaste).
		Methods(http.MethodPost)
	r.Use(OpenCORSMiddleware("/"))

	r.HandleFunc("/", h.handleHome).
		Methods(http.MethodGet)

	r.HandleFunc("/about", h.handleAbout).Methods(http.MethodGet)
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))

	// TODO add format filters
	r.HandleFunc("/s/{key}", h.handleShowPaste).Methods(http.MethodGet)
	r.HandleFunc("/r/{key}", h.handleShowRaw).Methods(http.MethodGet)
	r.HandleFunc("/md/{key}", h.HandleShowMarkdown).Methods(http.MethodGet)

	return r
}

// endregion

// region Middlewares

// OpenCORSMiddleware accepts requests from everywhere, on the
// OPTIONS and POST methods only.
func OpenCORSMiddleware(route string) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
			if request.Method == http.MethodPost && request.RequestURI == route {
				for key, value := range map[string]string{
					"Access-Control-Allow-Origin":  "*",
					"Access-Control-Allow-Methods": "OPTIONS, POST",
				} {
					response.Header().Set(key, value)
				}
			}

			next.ServeHTTP(response, request)
		})
	}
}

// endregion

// region Static pages

func (h Handlers) handleHome(w http.ResponseWriter, req *http.Request) {
	ErrIf(h.Templates.ExecuteTemplate(w, "homepage", nil))
}

func (h Handlers) handleAbout(w http.ResponseWriter, req *http.Request) {
	ErrIf(h.Templates.ExecuteTemplate(w, "about", nil))
}

// endregion

// region Form validators

// ValidateAndTransformPasteSubmitCommand validates the form based on the
// PasteSubmit schema, And fills a PasteSubmit command with associated values
func ValidateAndTransformPasteSubmitCommand(form url.Values) (
	PasteSubmitCommand, validator.Errors) {
	values, validationErrors := validator.Validator{Schema: PasteSubmitSchema}.
		Validate(form)

	command := PasteSubmitCommand{
		Paste:          values.Get("paste"),
		TimeStr:        values.Get("time"),
		ExpirationTime: 0,
	}

	if "" != command.TimeStr {
		keepaliveTime := time.Hour
		switch command.TimeStr {
		case "6h":
			keepaliveTime *= 6
		case "12h":
			keepaliveTime *= 12
		case "24h":
			keepaliveTime *= 24
		}
		command.ExpirationTime = keepaliveTime
	}

	return command, validationErrors
}

// endregion

// region Paste ephemeral handling

func (h Handlers) handleNewPaste(w http.ResponseWriter, req *http.Request) {
	err := req.ParseForm()

	if nil != err {
		h.PasteSubmitError(400, err, &Content{
			Error: "Form submit error",
		})(w, req)
		return
	}

	command, validationErrors := ValidateAndTransformPasteSubmitCommand(req.Form)

	if nil != validationErrors &&
		0 < len(validationErrors) {
		h.PasteSubmitError(400, validationErrors, &Content{
			Error:           "Form submit error",
			OriginalCommand: command,
		})(w, req)
		return
	}

	key := GetNewKey()

	err = h.BackendService.
		Persist(key, command.Paste, command.ExpirationTime)
	if nil != err {
		h.GenericHTTPError(500, err.Error())(w, req)
		return
	}

	http.Redirect(w, req, "/s/"+key, http.StatusFound)
	_, err = fmt.Fprint(w, key)
	ErrIf(err)
}

func (h Handlers) handleShowPaste(w http.ResponseWriter, req *http.Request) {
	key := mux.Vars(req)["key"]

	res, err := h.BackendService.Retrieve(key)
	if nil != err {
		h.GenericHTTPError(500, err.Error())(w, req)
		return
	} else if res == "" {
		h.GenericHTTPError(404, nil)(w, req)
		return
	}

	ErrIf(h.Templates.ExecuteTemplate(w, "show", &Content{
		Value: res,
		Raw:   "/r/" + key,
		Md:    "/md/" + key,
	}))
}

func (h Handlers) handleShowRaw(w http.ResponseWriter, req *http.Request) {
	key := mux.Vars(req)["key"]

	res, err := h.BackendService.Retrieve(key)
	if nil != err {
		h.GenericHTTPError(500, err.Error())(w, req)
		return
	} else if res == "" {
		h.GenericHTTPError(404, nil)(w, req)
		return
	}

	w.Header().Set("Content-Type", "text/plain; charset=utf8")
	_, err = fmt.Fprint(w, res)
	ErrIf(err)
}

// HandleShowMarkdown renders document with blackfriday.v2 and returns a HTML page
func (h Handlers) HandleShowMarkdown(w http.ResponseWriter, req *http.Request) {
	key := mux.Vars(req)["key"]

	res, err := h.BackendService.Retrieve(key)
	if nil != err {
		h.GenericHTTPError(500, err.Error())(w, req)
		return
	} else if res == "" {
		h.GenericHTTPError(404, nil)(w, req)
		return
	}

	ErrIf(h.Templates.ExecuteTemplate(w, "markdown", &Content{
		Value: res,
		Raw:   "/r/" + key,
		View:  "/s/" + key,
	}))
}

// endregion

// region Error handlers

// GenericHTTPError takes a status code and an error, and
// - logs the error using the global ErrIf method
// - writes the generic template `http-error` with the status code and message
func (h *Handlers) GenericHTTPError(
	statusCode int,
	error interface{},
) func(w http.ResponseWriter, req *http.Request) {
	ErrIf(error)

	return func(w http.ResponseWriter, req *http.Request) {
		w.WriteHeader(statusCode)
		ErrIf(h.Templates.ExecuteTemplate(w, "http-error", map[string]interface{}{
			"Error": genericErrors[statusCode],
		}))
	}
}

// PasteSubmitError takes a status code and a content dataset, and
// - logs the content.Error value using the global ErrIf method
// - writes the homepage template with the content
func (h *Handlers) PasteSubmitError(
	statusCode int,
	original interface{},
	content *Content,
) func(w http.ResponseWriter, req *http.Request) {
	ErrIf(original)

	return func(w http.ResponseWriter, req *http.Request) {
		w.WriteHeader(statusCode)
		ErrIf(h.Templates.ExecuteTemplate(w, "homepage", content))
	}
}

// endregion

package main

import (
	"github.com/spf13/viper"
)

// ProjectName is the binary name
var ProjectName = "paste"

// RedisConfig is the redis-specific configuration
type RedisConfig struct {
	Address  string
	Password string
	Database int
}

// HTTPConfig is the HTTP-specific configuration
type HTTPConfig struct {
	Host string
}

type PrometheusConfig struct {
	Enabled bool
	Host    string
}

// Config is the app config
type Config struct {
	Redis      RedisConfig
	HTTP       HTTPConfig
	Prometheus PrometheusConfig
}

// LoadConfig loads the config at the given path
// Loading order:
// - Environment
// - Configuration file
// - Default values
func LoadConfig(path string) (*Config, error) {
	// Default values, can be changed here
	config := &Config{
		Redis:      RedisConfig{},
		HTTP:       HTTPConfig{},
		Prometheus: PrometheusConfig{},
	}
	loader := viper.New()
	loader.SetConfigName(ProjectName)

	// Defaults
	for key, defaults := range map[string]interface{}{
		"Redis.Address":      "127.0.0.1:6379",
		"Redis.Password":     "",
		"Redis.Database":     0,
		"HTTP.Host":          "127.0.0.1:1234",
		"Prometheus.Enabled": false,
		"Prometheus.Host":    "127.0.0.1:1235",
	} {
		loader.SetDefault(key, defaults)
	}

	// Sources
	loader.AddConfigPath(path)
	loader.AutomaticEnv()

	err := loader.ReadInConfig()

	config.HTTP.Host = loader.GetString("Http.Host")
	config.Redis.Address = loader.GetString("Redis.Address")
	config.Redis.Password = loader.GetString("Redis.Password")
	config.Redis.Database = loader.GetInt("Redis.Database")
	config.Prometheus.Enabled = loader.GetBool("Prometheus.Enabled")
	config.Prometheus.Host = loader.GetString("Prometheus.Host")

	return config, err
}

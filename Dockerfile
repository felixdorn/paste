# Build step
FROM node:12-alpine as css-env
ADD . /src
WORKDIR /src

RUN yarn && \
    yarn build

FROM golang:alpine as build-env
ADD . /src
WORKDIR /src

ENV CGO_ENABLED 0
ENV GOOS linux

RUN apk add --no-cache git && \
    go version && \
    go env && \
    go get -v . && \
    go build -a -installsuffix cgo -o /paste .

# Runnable image
FROM scratch

MAINTAINER Artemis

WORKDIR /app
COPY --from=css-env /src/static /app/static
COPY --from=build-env /paste /app/paste
COPY --from=build-env /src/templates /app/templates
CMD ["/app/paste"]
# Port for HTTP server
EXPOSE 1234

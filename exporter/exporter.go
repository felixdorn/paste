package exporter

import (
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/log"
	"gitlab.com/Artemix/paste/storage"
	"net/http"
	"time"
)

// region Type declaration

const (
	namespace = "paste"
)

type Exporter struct {
	Backend storage.StatsBackend
	Server  *http.Server
	Host    string
}

func New(backend storage.StatsBackend, host string) Exporter {
	handlers := mux.NewRouter()

	handlers.Handle("/metrics", promhttp.Handler())
	exporter := Exporter{
		Backend: backend,
		Server: &http.Server{
			Addr:         host,
			ReadTimeout:  10 * time.Second,
			WriteTimeout: 10 * time.Second,
			Handler:      handlers,
		},
		Host: host,
	}

	prometheus.MustRegister(exporter)

	return exporter
}

// endregion

// region Prometheus stats

var (
	pasteCount = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "count"),
		"Count of currently stored pasteCount",
		nil, nil,
	)

	totalSize = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "total_paste_size"),
		"Total size (counting every paste) in bytes",
		nil, nil,
	)

	averageSize = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "average_paste_size"),
		"Average size (counting every paste) in bytes",
		nil, nil,
	)
)

func (e Exporter) Describe(descriptor chan<- *prometheus.Desc) {
	descriptor <- pasteCount
	descriptor <- totalSize
	descriptor <- averageSize
}

func (e Exporter) Collect(descriptor chan<- prometheus.Metric) {
	stats, err := e.Backend.RetrieveStats()
	if err != nil {
		log.Errorf("StatsBackend error with stats collection: %s", err.Error())
		return
	}

	storeMetric(descriptor, stats.PasteCount, pasteCount)
	storeMetric(descriptor, stats.AverageSize, averageSize)
	storeMetric(descriptor, stats.TotalSize, totalSize)
}

func storeMetric(ch chan<- prometheus.Metric, value float64, desc *prometheus.Desc, labels ...string) {
	ch <- prometheus.MustNewConstMetric(desc, prometheus.GaugeValue, value, labels...)
}

// endregion

package storage

import "time"

// region Public Backend interface

type Backend interface {
	// Tries to store the paste at the given key, with the given expiration
	// If error isn't nil, there was a fatal error on redis querying
	Persist(key string, value string, expiration time.Duration) error
	// Tries to retrieve a paste at the given key
	// If string is not empty, the paste is found
	// If string is empty but error is nil, the paste was not found but the request succeeded
	// If string is empty and error isn't nil, there was a fatal error on redis querying
	Retrieve(key string) (string, error)
}

type Stats struct {
	PasteCount  float64
	TotalSize   float64
	AverageSize float64
}

type StatsBackend interface {
	RetrieveStats() (*Stats, error)
}

// endregion

package storage

import (
	"fmt"
	"github.com/go-redis/redis"
	"time"
)

const prefix = "PASTE::"

// region Base type

type RedisBackend struct {
	Client *redis.Client
}

func NewRedisBackend(config *redis.Options) (*RedisBackend, error) {
	backend := &RedisBackend{Client: redis.NewClient(config)}
	return backend, backend.Client.Ping().Err()
}

// endregion

// region Backend interface implementation

func prefixed(key string) string {
	return prefix + key
}

func (r *RedisBackend) Persist(key string, value string, expiration time.Duration) error {
	return r.Client.
		Set(prefixed(key), value, expiration).
		Err()
}

func (r *RedisBackend) Retrieve(key string) (string, error) {
	res := r.Client.Get(prefixed(key))

	if err := res.Err(); err != nil {
		if err == redis.Nil {
			// Not found
			return "", nil
		} else {
			// Crash
			return "", err
		}
	}

	// Found
	return res.Val(), nil
}

// endregion

// region Stats backend interface implementation

func (r *RedisBackend) RetrieveStats() (*Stats, error) {
	keys, err := r.Client.Keys(fmt.Sprintf("%s*", prefix)).Result()
	if err != nil {
		return nil, err
	}

	keyCount := len(keys)
	res := Stats{
		PasteCount: float64(keyCount),
		TotalSize:  0,
	}

	sizes := make([]float64, keyCount)
	index := 0
	for _, key := range keys {
		sizes[index] = float64(r.Client.MemoryUsage(key, 0).Val())
		res.TotalSize += sizes[index]

		index += 1
	}
	res.AverageSize = averageFloat64(sizes)

	return &res, nil
}

func averageFloat64(data []float64) float64 {
	if 0 == len(data) {
		return 0
	}

	total := float64(0)

	for _, data := range data {
		total += data
	}

	return total / float64(len(data))
}

// endregion

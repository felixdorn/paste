module gitlab.com/Artemix/paste

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/gorilla/mux v1.7.3
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/onsi/ginkgo v1.10.3 // indirect
	github.com/onsi/gomega v1.7.1 // indirect
	github.com/prometheus/client_golang v1.2.1
	github.com/prometheus/common v0.7.0
	github.com/spf13/viper v1.3.2
	gitlab.com/Artemix/validator-go v0.0.4
	gitlab.com/golang-commonmark/markdown v0.0.0-20191127184510-91b5b3c99c19
)

go 1.13
